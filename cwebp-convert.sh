#!/usr/bin/env bash
# An automatic lossless conversion from PNG and JPG to webp

# Show usage and exit with a particular error and exit code
function show_usage ()
{
	>&2 echo "USAGE: $0 FILE..."
}

# check if there were no arguments provided or if show_usage was
if [ $# == 0]; then
	show_usage
elif [ $1 == "show_usage" ]; then
	show_usage
fi

# if we cannot run cwebp then exit
if [ -z $(which cwebp) ]; then
	echo "Unable to find cwebp"
	exit 1
fi 

# the main code
for file in "$@"; do
	output_dir=$(dirname "${file}") #get the dir
	output_filename=$(basename "${file}") # get the basename
	output_filename=${output_filename%.*} # remove the last extension
	output_file="${output_dir}/${output_filename}" 
	cwebp -lossless ${file} -o ${output_file}.webp # run the conversion
done